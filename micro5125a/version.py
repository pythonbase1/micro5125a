# -*- coding: utf-8 -*-

""" micro5125a/version.py """
__version__ = "0.1.4"

# 0.1.4 (30/07/2021): Correct real time (rt) query of cli software
# 0.1.3 (01/06/2021): Add phase rate choice in continous acquisition mode.
# 0.1.2 (05/05/2021): Use of OrderedDict instead of dict to correct bug with
#                     Python version < 3.7.
# 0.1.1 (03/05/2021): Correct parsing problem of adev outpouts when length of
#                     adev value and adev noise floor is different.
#                     Also correct display problem of argument parser.
# 0.1.0 (16/10/2019): First version.
